// ==UserScript==
// @name        elisaviihde-url
// @namespace   https://github.com/hoxu/
// @description Show Elisa Viihde streaming URL
// @include     https://elisaviihde.fi/etvrecorder/program.sl?programid=*
// @include     http://elisaviihde.fi/etvrecorder/program.sl?programid=*
// @version     3
// @grant       none
// ==/UserScript==

// Get stream URL
var url = document.getElementsByTagName("script")[13].outerHTML.match('http://[^"]*');

console.log('elisaviihde-url: ' + url);

// Create link to it
var ele = document.createElement('div');
ele.style = 'text-align: center';
var eleA = document.createElement('a');
eleA.href = url;
eleA.innerHTML = 'Stream URL';
ele.appendChild(eleA);

document.getElementsByClassName('vlcplayer')[0].appendChild( ele );

// IMDB link
var h2 = $('.view_info h2');
var movieTitle = h2.text();
movieTitle = movieTitle.replace('Elokuva:', '').trim();

var imdbA = document.createElement('a');
imdbA.href = 'http://www.imdb.com/find?q=' + movieTitle + '&s=all';
imdbA.innerHTML = ' [IMDb]';
h2.append(imdbA);

